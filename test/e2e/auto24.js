/*
[x] open auto24.ee
[x] change windowsize
[x] wait for body
[x] Title auto24.ee - uued ja kasutatud sõidukid"
[x] take screenshot
[x] page contains "Audi"
[x] click "audi"
[x] take screenshot
[x] page contains "100"
[x] click "100"
[x] take screenshot
[x] page contains "vaata kõiki"
[x] click "vaata kõiki"
[x] take screenshot
[x] page contains "Avalehit"
[x] click "Avaleht"
[x] take screenshot
*/

const config = require('../../nightwatch.conf.js');

module.exports = {
  Auto24(browser) {
    browser
      .url('https://www.auto24.ee/main/mainindex.php')
      .windowSize('current', 1920, 1080)
      .waitForElementVisible('body')
      .assert.title('auto24.ee - uued ja kasutatud sõidukid')
      .saveScreenshot(`${config.imgpath(browser)}auto24_koduleht.png`)
      .assert.containsText('.tpl-body', 'Audi')
      .useXpath()
      .assert.containsText("//a[text()='Audi']", 'Audi')
      .click("//a[text()='Audi']")
      .saveScreenshot(`${config.imgpath(browser)}audi.png`)
      .assert.containsText("//a[text()='100']", '100')
      .click("//a[text()='100']")
      .assert.containsText("//a[text()='vaata kõiki']", 'vaata kõiki')
      .click("//a[text()='vaata kõiki']")
      .saveScreenshot(`${config.imgpath(browser)}vanad_audid.png`)
      .pause(50)
      .assert.containsText("//a[text()='Avaleht']", 'Avaleht')
      .click("//a[text()='Avaleht']")
      .saveScreenshot(`${config.imgpath(browser)}koduleht_uuesti.png`)
      .end();
  },
};
